// Onload
window.onload = function() {
   //Carousel 
  function e(a) {
    var b = document.getElementsByClassName("carousel");
    a > b.length && (c = 1);
    1 > a && (c = b.length);
    for (a = 0;a < b.length;a++) {
      //Escondendo Slide
      b[a].style.display = "none"; 
    }
      //Mostando Slide
    b[c - 1].style.display = "block";
  }
  //Alterando slide carousel 
  for (var d = document.getElementsByClassName("arrow"), b = 0;b < d.length;b++) {
    d[b].addEventListener("click", function() {
      //Chamando Função para alterar Slide
      e(c += parseInt(this.getAttribute("data-nav")));
    });
  }
  var c = 1;
  e(c);
    //Accordion
  d = document.getElementsByClassName("accordion");
  for (b = 0;b < d.length;b++) {
    d[b].onclick = function() {
      //Adicionando Classe Ativa
      this.classList.toggle("active");
      //Expandido Accordion
      var a = this.nextElementSibling;
      a.style.maxHeight = a.style.maxHeight ? null : a.scrollHeight + "px";
    };
  }
    //Touch Screen - Chamando função para alterar slide carousel
  document.getElementById("touch").addEventListener("touchstart", function(a) {
    e(c += 1); // Chamando função para alternar slide carousel
  }, !1);
  document.querySelector(".input").onchange = function() {
    //Validando por Onchange cada input modificado do formulario
    0 == this.checkValidity() ? document.getElementById("" + this.id).parentElement.classList.add("has-error") : document.getElementById("" + this.id).parentElement.classList.remove("has-error");
  };
  //Validando Formulário
  document.getElementById("submit").onclick = function() {
    var a = document.forms["contact-form"].elements, c = 0;
    //Percorrendo todos elementos do formulário  verificando validade e adicionando/removendo classe css de erro.
    for (b = 0;b < a.length;b++) {
      console.log(a), 0 == a[b].checkValidity() ? a[b].parentElement.classList.add("has-error") : (c++, a[b].parentElement.classList.remove("has-error"));
    }
    //Enviando Formulário
    c == a.length && document.getElementById("contact-form").submit();
  };
  //Mascara Input Telefone
  document.getElementById("telefone").onkeyup = function() {
    a: {
      var a = event;
      try {
        var b = document.getElementById("telefone"), c = b.value;
        try {
          var d = a.which ? a.which : event.keyCode;
          if (46 == d || 8 == d) {
            b.value = "";
            break a;
          }
        } catch (h) {
        }
        for (var a = /[0\*]/, d = /[0-9]/, e = "", g = 0, f = 0;15 > f && !(f >= c.length) && ("0" != "(00) 00000-0000"[f] || null != c[g].match(d));) {
          for (;null == "(00) 00000-0000"[f].match(a) && c[g] != "(00) 00000-0000"[f];) {
            e += "(00) 00000-0000"[f++];
          }
          e += c[g++];
          f++;
        }
        b.value = e;
      } catch (h) {
      }
    }
  };
};